#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <regex.h>
#include <stdlib.h>

#include "defs.h"
#include "server.h"

int main(int argc, char** argv) {
    // Assert that we have all the positional arguments
    assert(argc >= 3);

    uint16_t srv_port = strtol(argv[2], NULL, 10);

    struct sockaddr_in bind_info;
    bind_info.sin_family = AF_INET;
    bind_info.sin_port = htons(srv_port);
    if (inet_pton(AF_INET, argv[1], &bind_info.sin_addr) != 1) {
        printf("Invalid IPv4 address or port specified.\n");
        return -1;
    }

    return run_server(bind_info);
}