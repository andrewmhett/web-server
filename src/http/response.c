#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include "../defs.h"

void send_error(int client_fd, const char* error_string) {
    write(client_fd, HTTP_RESP_BEGIN, RESP_BEGIN_LEN);
    write(client_fd, error_string, strlen(error_string));
    write(client_fd, CRLF, CRLF_LEN);
    write(client_fd, CRLF, CRLF_LEN);
}

void send_object(int client_fd, FILE* obj_file, const char* obj_type) {
    write(client_fd, HTTP_RESP_BEGIN, RESP_BEGIN_LEN);
    write(client_fd, HTTP_OK, OK_LEN);
    write(client_fd, CRLF, CRLF_LEN);
    write(client_fd, "Content-Type: ", 14);
    write(client_fd, obj_type, strlen(obj_type));
    write(client_fd, CRLF, CRLF_LEN);
    // Determine the size of the file
    size_t content_length = 0;
    while (getc(obj_file) != EOF) content_length++;
    char length_string[10];
    sprintf(length_string, "%zu", content_length);
    write(client_fd, "Content-Length: ", 16);
    write(client_fd, length_string, strlen(length_string));
    write(client_fd, CRLF, CRLF_LEN);
    write(client_fd, CRLF, CRLF_LEN);
    // Now send the object's data
    rewind(obj_file);
    uint8_t curr_byte;
    for (int byte = 0; byte < content_length; byte++) {
        curr_byte = (uint8_t)getc(obj_file);
        write(client_fd, &curr_byte, 1);
    }
}