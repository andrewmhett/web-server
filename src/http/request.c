#include <regex.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "../defs.h"
#include "request.h"
#include "response.h"

http_request process_http_request(const char* http_message) {
    http_request request;
    // Compile regular expressions for sifting through messages
    regex_t http_req_regp;
    assert(regcomp(&http_req_regp, HTTP_REQ_REGEX, REG_EXTENDED) == 0);

    regmatch_t matches[MAX_REQ_MATCHES];
    if (regexec(&http_req_regp, http_message, MAX_REQ_MATCHES, matches, 0) == REG_NOMATCH) {
        request.method = HTTP_INVALID;
        return request;
    }
    size_t match_num = 0;
    size_t curr_match = -1;
    while (match_num < http_req_regp.re_nsub) {
        curr_match++;
        if (matches[curr_match].rm_so == -1) {
            continue;
        }
        size_t match_length = matches[curr_match].rm_eo - matches[curr_match].rm_so;
        //   |----2-----|    |3|                     |--4--|          |5|
        // ^((GET)|(POST))\s(.*)\sHTTP\/[0-9]\.[0-9](\r\n.*)*\r\n\r\n(.*)
        switch (match_num){
            case 0: // Whole message
            case 1: // Unused (grouping)
                break;
            case 2: // HTTP Method
                if (strncmp(http_message + matches[curr_match].rm_so, "GET", 3) == 0) request.method = HTTP_GET;
                if (strncmp(http_message + matches[curr_match].rm_so, "POST", 4) == 0) request.method = HTTP_POST;
                break;
            case 3: // Requested filename
                request.filename = (char*) malloc(match_length + 1);
                if (match_length == 1) {
                    strcpy(request.filename, INDEX_PAGE);
                    break;
                }
                memcpy(request.filename, http_message + matches[curr_match].rm_so, match_length);
                break;
            case 4: // Headers
                request.headers = parse_headers(http_message + matches[curr_match].rm_so);
                break;
            case 5: // Body
                if (match_length == 0) {
                    request.body = NULL;
                    break;
                }
                request.body = (char*) malloc(match_length + 1);
                memcpy(request.body, http_message + matches[curr_match].rm_so, match_length);
                break;
        }
        match_num++;
    }
    return request;
}

void get_content_type(http_request request, char* content_type) {
    for (int i = strlen(request.filename) - 1; i >= 0; i--) {
        if (request.filename[i] == '.') { // beginning of extension
            i++;
            if (strcasecmp(request.filename + i, "HTML") == 0) {
                sprintf(content_type, "text/html");
                break;
            }
            if (strcasecmp(request.filename + i, "JPG") == 0
                || strcasecmp(request.filename + i, "JPEG") == 0) {
                sprintf(content_type, "image/jpeg");
                break;
            }
            if (strcasecmp(request.filename + i, "PNG") == 0) {
                sprintf(content_type, "image/png");
                break;
            }
            // Add more content types here
            break;
        }
    }
}

void process_get(int client_fd, http_request request) {
    // Make sure that the requested file exists
    int full_length = strlen(SERVER_DIR_BASE) + strlen(request.filename);
    char full_filename[full_length + 1];
    sprintf(full_filename, "%s%s", SERVER_DIR_BASE, request.filename);
    full_filename[full_length] = 0;

    FILE* obj_file = fopen(full_filename, "r");

    if (obj_file == NULL) {
        // The requested file could not be found
        send_error(client_fd, HTTP_NOT_FOUND);
        return;
    }

    char content_type[10];
    get_content_type(request, content_type);

    send_object(client_fd, obj_file, content_type);

    fclose(obj_file);
}

void process_post(http_request request) {}