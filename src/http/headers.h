#ifndef HEADERS_H
#define HEADERS_H

#include <regex.h>

#include "../defs.h"

#define MAX_HEAD_MATCHES        2
#define ALLOW_REGEX             "Accept: ([^\r]*)"
#define AUTHORIZATION_REGEX     "Authorization: ([^\r]*)"
#define CONTENT_ENCODING_REGEX  "Content-Encoding: ([^\r]*)"
#define CONTENT_LENGTH_REGEX    "Content-Length: ([^\r]*)"
#define DATE_REGEX              "Date: ([^\r]*)"
#define EXPIRES_REGEX           "Expires: ([^\r]*)"
#define FROM_REGEX              "From: ([^\r]*)"
#define IF_MOD_SINCE_REGEX      "If-Modified-Since: ([^\r]*)"
#define LAST_MODIFIED_REGEX     "Last-Modified: ([^\r]*)"
#define PRAGMA_REGEX            "Pragma: ([^\r]*)"
#define REFERER_REGEX           "Referer: ([^\r]*)"
#define USER_AGENT_REGEX        "User-Agent: ([^\r]*)"

typedef struct headers {
    char* allow;
    char* authorization;
    char* content_encoding;
    char* content_length;
    char* date;
    char* expires;
    char* from;
    char* if_modified_since;
    char* last_modified;
    char* pragma;
    char* referer;
    char* user_agent;
} headers;

void parse_header(const char*, regex_t*, char**);
void free_headers(headers);
headers parse_headers(const char*);

#endif