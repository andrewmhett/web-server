#include <regex.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "headers.h"

void parse_header(const char* headers_string, regex_t* regp, char** value_string) {
    regmatch_t matches[MAX_HEAD_MATCHES];
    if (regexec(regp, headers_string, MAX_HEAD_MATCHES, matches, 0) == REG_NOMATCH) return;
    size_t value_length = matches[1].rm_eo - matches[1].rm_so;
    *value_string = (char*) malloc(value_length + 1);
    (*value_string)[value_length] = 0;
    memcpy(*value_string, headers_string + matches[1].rm_so, value_length);
}

void free_headers(headers header_struct) {
    if (header_struct.allow != NULL)                free(header_struct.allow);
    if (header_struct.authorization != NULL)        free(header_struct.authorization);
    if (header_struct.content_encoding != NULL)     free(header_struct.content_encoding);
    if (header_struct.content_length != NULL)       free(header_struct.content_length);
    if (header_struct.date != NULL)                 free(header_struct.date);
    if (header_struct.expires != NULL)              free(header_struct.expires);
    if (header_struct.from != NULL)                 free(header_struct.from);
    if (header_struct.if_modified_since != NULL)    free(header_struct.if_modified_since);
    if (header_struct.last_modified != NULL)        free(header_struct.last_modified);
    if (header_struct.pragma != NULL)               free(header_struct.pragma);
    if (header_struct.referer != NULL)              free(header_struct.referer);
    if (header_struct.user_agent != NULL)           free(header_struct.user_agent);
}

headers parse_headers(const char* headers_string) {
    headers header_struct;
    memset(&header_struct, 0, sizeof(headers));

    // Define regular expressions
    regex_t allow_header_regp;
    regex_t authorization_header_regp;
    regex_t content_encoding_header_regp;
    regex_t content_length_header_regp;
    regex_t date_header_regp;
    regex_t expires_header_regp;
    regex_t from_header_regp;
    regex_t if_mod_since_header_regp;
    regex_t last_modified_header_regp;
    regex_t pragma_header_regp;
    regex_t referer_header_regp;
    regex_t user_agent_header_regp;

    // Compile regular expressions
    assert(regcomp(&allow_header_regp,              ALLOW_REGEX,            REG_EXTENDED) == 0);
    assert(regcomp(&authorization_header_regp,      AUTHORIZATION_REGEX,    REG_EXTENDED) == 0);
    assert(regcomp(&content_encoding_header_regp,   CONTENT_ENCODING_REGEX, REG_EXTENDED) == 0);
    assert(regcomp(&content_length_header_regp,     CONTENT_LENGTH_REGEX,   REG_EXTENDED) == 0);
    assert(regcomp(&date_header_regp,               DATE_REGEX,             REG_EXTENDED) == 0);
    assert(regcomp(&expires_header_regp,            EXPIRES_REGEX,          REG_EXTENDED) == 0);
    assert(regcomp(&from_header_regp,               FROM_REGEX,             REG_EXTENDED) == 0);
    assert(regcomp(&if_mod_since_header_regp,       IF_MOD_SINCE_REGEX,     REG_EXTENDED) == 0);
    assert(regcomp(&last_modified_header_regp,      LAST_MODIFIED_REGEX,    REG_EXTENDED) == 0);
    assert(regcomp(&pragma_header_regp,             PRAGMA_REGEX,           REG_EXTENDED) == 0);
    assert(regcomp(&referer_header_regp,            REFERER_REGEX,          REG_EXTENDED) == 0);
    assert(regcomp(&user_agent_header_regp,         USER_AGENT_REGEX,       REG_EXTENDED) == 0);

    parse_header(headers_string, &allow_header_regp,            &header_struct.allow);
    parse_header(headers_string, &authorization_header_regp,    &header_struct.authorization);
    parse_header(headers_string, &content_encoding_header_regp, &header_struct.content_encoding);
    parse_header(headers_string, &content_length_header_regp,   &header_struct.content_length);
    parse_header(headers_string, &date_header_regp,             &header_struct.date);
    parse_header(headers_string, &expires_header_regp,          &header_struct.expires);
    parse_header(headers_string, &from_header_regp,             &header_struct.from);
    parse_header(headers_string, &if_mod_since_header_regp,     &header_struct.if_modified_since);
    parse_header(headers_string, &last_modified_header_regp,    &header_struct.last_modified);
    parse_header(headers_string, &pragma_header_regp,           &header_struct.pragma);
    parse_header(headers_string, &referer_header_regp,          &header_struct.referer);
    parse_header(headers_string, &user_agent_header_regp,       &header_struct.user_agent);

    return header_struct;
}