#ifndef HTTP_METHOD_H
#define HTTP_METHOD_H

typedef enum http_method {
    HTTP_INVALID,
    HTTP_GET,
    HTTP_POST
} http_method;

#endif