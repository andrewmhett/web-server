#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H

#include "headers.h"
#include "method.h"

typedef struct http_request {
    http_method method;
    char* filename;
    headers headers;
    char* body;
} http_request;

http_request process_http_request(const char*);
void process_get(int, http_request);
void process_post(http_request);

#endif