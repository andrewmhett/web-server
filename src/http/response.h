#ifndef RESPONSE_H
#define RESPONSE_H

#include <stdio.h>

void send_error(int, const char*);
void send_object(int, FILE*, const char*);

#endif