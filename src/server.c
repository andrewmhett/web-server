#include <sys/socket.h>
#include <arpa/inet.h>
#include <regex.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>

#include "defs.h"
#include "server.h"
#include "http/request.h"

int run_server(struct sockaddr_in bind_info) {
    // Assign the server a file descriptor for its socket
    int srv_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    assert(srv_socket_fd != -1);

    // Bind the server's socket using the binding info passed to us
    if (bind(srv_socket_fd, (struct sockaddr*)&bind_info, sizeof(bind_info)) == -1) {
        if (errno == EACCES) {
            printf("Insufficient permissions to bind port. Binding ports below 1024 requires superuser access.\n");
            return -2;
        } else {
            printf("Errno %d encountered while attempting to bind socket.\n", errno);
            return -3;
        }
    }

    // Mark this socket as accepting incoming connections
    assert(listen(srv_socket_fd, 1) != -1);

    printf("Server running on port %d\n", htons(bind_info.sin_port));

    while (1) {
        // Wait for and accept a single incoming connection
        int client_fd = accept(srv_socket_fd, NULL, NULL);
        assert(client_fd != -1);

        char msg_buf[MAX_MESSAGE_LEN + 1];
        int incoming_bytes = -1;
        while (incoming_bytes != 0) {
            // Read bytes sent to us by the client
            incoming_bytes = read(client_fd, msg_buf, MAX_MESSAGE_LEN);
            msg_buf[incoming_bytes] = 0;
            http_request req;
            req.filename = NULL;
            req.body = NULL;
            req = process_http_request(msg_buf);
            switch (req.method) {
                case HTTP_INVALID:
                    printf("INVALID MESSAGE\n");
                    break;
                case HTTP_GET:
                    process_get(client_fd, req);
                    break;
                case HTTP_POST:
                    process_post(req);
                    break;
            }
            if (req.filename != NULL) free(req.filename);
            if (req.body != NULL) free(req.body);
            free_headers(req.headers);
            close(client_fd);
            incoming_bytes = 0;
        }
    }
    return 0;
}