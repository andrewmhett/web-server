#ifndef DEFS_H
#define DEFS_H

// Server Configuration
#define SERVER_DIR_BASE "example_site" // filesystem path to site source files
#define INDEX_PAGE      "/index.html"
#define MAX_MESSAGE_LEN 2048

// HTTP Defines
#define MAX_REQ_MATCHES 20
#define CRLF            "\r\n"
#define CRLF_LEN        2
#define HTTP_RESP_BEGIN "HTTP/1.0 "
#define RESP_BEGIN_LEN  9
#define HTTP_NOT_FOUND  "404 Not Found"
#define NOT_FOUND_LEN   13
#define HTTP_NOT_IMPL   "400 Not Implemented"
#define NOT_IMPL_LEN    19
#define HTTP_OK         "200 OK"
#define OK_LEN          6


// Regular Expressions
#define METHOD_REGEX    "(GET)|(POST)"
#define HTTP_REQ_REGEX  "^(" METHOD_REGEX ") (.*) HTTP\\/[0-9]\\.[0-9](" CRLF ".*)*" CRLF CRLF "(.*)"

#endif