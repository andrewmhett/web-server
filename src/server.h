#ifndef SERVER_H
#define SERVER_H

#include <arpa/inet.h>

int run_server(struct sockaddr_in);

#endif