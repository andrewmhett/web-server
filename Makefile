GCC		= gcc
CFLAGS		= -O3
SRCDIR		= src
SRCS		= $(SRCDIR)/*.c $(SRCDIR)/*.h $(SRCDIR)/http/*.c $(SRCDIR)/http/*.h
IMPLS		= $(SRCDIR)/*.c $(SRCDIR)/http/*.c
BUILDDIR	= build
BIN		= $(BUILDDIR)/server

.PHONY: clean

$(BIN): $(SRCS) $(BUILDDIR)
	$(GCC) $(IMPLS) $(CFLAGS) -o $@

$(BUILDDIR):
	mkdir $(BUILDDIR)

clean:
	if [ -e $(BUILDDIR) ]; then rm -r $(BUILDDIR); fi
